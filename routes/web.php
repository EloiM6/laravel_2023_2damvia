<?php

use App\Http\Controllers\ControllerApp;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome',function(){
   return "Espero solo un 10 por vuestra parte!!";
});

Route::get('/getAllPolitics',[ControllerApp::class,'getAllPolitics']);
Route::get('/getMascotaPolitic/{id}',[ControllerApp::class,'getMascotaPolitic']);
Route::get('/setCarrec/{id}/{carrec}',[ControllerApp::class,'setCarrec']);
Route::get('/addEvento/{patata}',[ControllerApp::class,'addEvent']);
Route::get('/getPoliticsCarrec/{carrec}',[ControllerApp::class,'getPoliticsCarrec']);
Route::get('/removeEvent/{id}',[ControllerApp::class,'removeEvent']);
Route::get('/getPoliticsByNomCognoms/{nom}/{cognoms}',[ControllerApp::class,'getPoliticsByNomCognoms']);
Route::get('/getEventsPolitics/{id}',[ControllerApp::class,'getEventsPolitics']);
Route::get('/setEventPolitic/{idPolitic}/{idEvent}',[ControllerApp::class,'setEventPolitic']);
Route::get('/holaBlade',function(){
    return view('prova1');
});

Route::get('/getViewAllPolitics',[ControllerApp::class,'getFormAllPolitics']);
Route::get('/getViewAllMascotes',[ControllerApp::class,'getFormAllMascotes']);
Route::get('/dadesUsuari',[ControllerApp::class,'dadesUsuari']);
Route::get('/newInforme', [ControllerApp::class,'entregarInforme']);
Route::post('/newInforme', [ControllerApp::class,'pujarInforme']);
Route::get('/meusInformes', [ControllerApp::class,'meusInformes']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
