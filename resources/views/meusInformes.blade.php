<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous" defer></script>
</head>
<body>
@section('content')
    <div class="container">
        <!-- si estas logueado -->
        @if (Auth::check())

            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                <!-- això ho faig així perquè hi ha prova1 a l'enllaç
                sino amb <a href="uploads/{ {  Session::get('file') }}">Practica</a>
                ja funciona-->
                <a href="uploads/{{ Session::get('file') }}">Informe</a>
            @endif


            <table class="table">
                <thead><tr>
                    <th colspan="2">Informes</th>
                </tr>
                </thead>
                <tbody>
                {{$user->id}}
                @foreach($informes as $inf)

                    <tr>
                        <td>
                            {{$inf->nom}}
                        </td>
                        <td>
                            {{$inf->created_at}}
                        </td>
                        <td>
                            {{$inf->idPolitic}}
                        </td>
                        <td>
                            {{$inf->userid}}
                        </td>
                        <td>

                            <a href="uploads/{{$inf->fitxer}}">Descarregar</a>
                        </td>

                    </tr>


                @endforeach</tbody>
            </table>
            <a href="/newInforme" class="btn btn-primary">Enviar Informe</a>

        @else
            <h3>You need to log in. <a href="/login">Click here to login</a></h3>
        @endif

    </div>
@show


</body>
</html>
