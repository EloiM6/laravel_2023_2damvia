<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous" defer></script>
</head>
<body>

@section('content')
    <div class="container">
        <h2>Add New Informe</h2>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>ERROR!</strong> T'has equivocat!!!
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="/newInforme" enctype="multipart/form-data">

            <div class="form-group">
                Nom Informe: <input type="text" name="nom" class="form-control"></input>
            </div>
            <br/>
            <div class="form-group">
                Id Politic: <input type="number" name="politic" class="form-control">
            </div>
            <br/>
            <div class="form-group">
                Nom arxiu: <input type="file" name="file" class="form-control">
            </div>


            <div class="form-group">
                <button type="submit" class="btn btn-primary">Entregar Informe</button>
            </div>
            {{ csrf_field() }}
        </form>


    </div>


</body>
</html>
