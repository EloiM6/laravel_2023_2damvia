<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
<div class="container">
    <h1 class="h1">Llistat Politics</h1>
    @if(count($tots)>0)
        <ul>
            @foreach($tots as $element)
                <li>Nom: {{ $element->nom}} Cognoms: {{ $element->cognoms}}</li>

            @endforeach
        </ul>
    @else
        <li>No hi ha elements</li>
    @endif
</div>
@endsection
</body>
</html>
