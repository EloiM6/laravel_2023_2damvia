<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EsdevenimentsPoliticsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('esdeveniments_politics')->insert([
            'idPolitic' => '1',
            'idEsdeveniment' => '3',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('esdeveniments_politics')->insert([
            'idPolitic' => '1',
            'idEsdeveniment' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('esdeveniments_politics')->insert([
            'idPolitic' => '2',
            'idEsdeveniment' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('esdeveniments_politics')->insert([
            'idPolitic' => '3',
            'idEsdeveniment' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
