<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PoliticsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('politics')->insert([
            'nom' => 'M.',
            'cognoms' => 'Rajoy Brey',
            'carrec' => 'Vecino',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
            ]);
        DB::table('politics')->insert([
            'nom' => 'Carles',
            'cognoms' => 'Puigdemont',
            'carrec' => 'Ausente',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('politics')->insert([
            'nom' => 'Emmanuelle',
            'cognoms' => 'Macron',
            'carrec' => 'Presi de Francia',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('politics')->insert([
            'nom' => 'Tony',
            'cognoms' => 'Pulido',
            'carrec' => 'Ausente',
            'corrupte' => false,
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
