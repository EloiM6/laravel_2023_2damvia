<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EsdevenimentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('esdeveniments')->insert([
            'nom' => 'Evento Escalera',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('esdeveniments')->insert([
            'nom' => 'Oscar for President',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('esdeveniments')->insert([
            'nom' => 'Inaugurar el AVE Huelva-Murcia',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
