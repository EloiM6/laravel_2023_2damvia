<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MascotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('mascotes')->insert([
            'nom' => 'Blanquita',
            'especie' => 'Cabra',
            'idPolitic' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('mascotes')->insert([
            'nom' => 'Montoro',
            'especie' => 'Toro',
            'idPolitic' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('mascotes')->insert([
            'nom' => 'Porro',
            'especie' => 'Sapo',
            'idPolitic' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
