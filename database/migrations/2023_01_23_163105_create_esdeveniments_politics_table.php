<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('esdeveniments_politics', function (Blueprint $table) {
            $table->bigIncrements("ideve_pol");
            $table->foreignId('idPolitic')->nullable()->constrained('politics')->references('idPolitic');
            $table->foreignId('idEsdeveniment')->nullable()->constrained('esdeveniments')->references('idEsdeveniment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('esdeveniments_politics', function (Blueprint $table) {
            $table->dropForeign(['esdeveniments_politics_idPolitic_foreign']);
            $table->dropColumn('idPolitic');
            $table->dropForeign(['esdeveniments_politics_idEsdeveniment_foreign']);
            $table->dropColumn('idEsdeveniment');
        });
        Schema::dropIfExists('esdeveniments_politics');
    }
};
