<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('mascotes', function (Blueprint $table) {
            $table->bigIncrements("idmascota");
            $table->string("nom",30);
            $table->string("especie",50);
            $table->foreignId('idPolitic')->nullable()->constrained('politics')->references('idPolitic');
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mascotes', function (Blueprint $table) {
            $table->dropForeign(['mascotes_idPolitic_foreign']);
            $table->dropColumn('idPolitic');
        });
        Schema::dropIfExists('mascotes');
    }
};
