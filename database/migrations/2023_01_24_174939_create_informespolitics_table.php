<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('informes_politics', function (Blueprint $table) {
            $table->bigIncrements("idInforme");
            $table->string("nom",200);
            $table->text("fitxer");
            $table->foreignId('idPolitic')->nullable()->constrained('politics')->references('idPolitic');
            $table->foreignId('userid')->nullable()->constrained('users')->references('id');
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informes_politics', function (Blueprint $table) {
            $table->dropForeign(['informes_politics_idPolitic_foreign']);
            $table->dropColumn('idPolitic');
            $table->dropForeign(['informes_politics_userid_foreign']);
            $table->dropColumn('userid');
        });
        Schema::dropIfExists('informespolitics');
    }
};
