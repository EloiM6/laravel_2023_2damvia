<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Informespolitics extends Model
{
    use HasFactory;
    protected $table = "informes_politics";
    protected $primaryKey = "idInforme";
    protected $fillable = ["idInforme","nom","fitxer"];
    public function politics(){
        return $this->BelongsTo(Politics::class, 'idPolitic', 'idPolitic');
    }
    public function users(){
        return $this->BelongsTo(User::class, 'userid', 'id');
    }
}
