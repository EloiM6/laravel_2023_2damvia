<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mascotes extends Model
{
    use HasFactory;
    protected $table = "mascotes";
    protected $primaryKey = "idMascota";
    protected $fillable = ["idMascota","nom","especie"];
    public function politics(){
        return $this->BelongsTo(Politics::class, 'idPolitic', 'idPolitic');
    }
}
