<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Esdeveniments extends Model
{
    use HasFactory;
    protected $table = "esdeveniments";
    protected $primaryKey = "idEsdeveniment";
    protected $fillable = ["idEsdeveniment","nom"];

    public function politics(){
        return $this->belongsToMany(Politics::class, "esdeveniments_politics","idEsdeveniment","idPolitic")->withTimestamps();
    }
}
