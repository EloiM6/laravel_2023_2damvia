<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Politics extends Model
{
    use HasFactory;
    protected $table = "politics";
    protected $primaryKey = "idPolitic";
    protected $fillable = ["idPolitic", 'nom', "cognoms",'carrec','corrupte'];

    public function mascotes(){
        return $this->hasMany(Mascotes::class);
    }

    public function esdeveniments(){
        return $this->belongsToMany(Esdeveniments::class,"esdeveniments_politics","idPolitic","idEsdeveniment")->withTimestamps();
    }


}
