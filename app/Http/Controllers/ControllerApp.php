<?php

namespace App\Http\Controllers;

use App\Models\Esdeveniments;
use App\Models\Informespolitics;
use App\Models\Mascotes;
use App\Models\Politics;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function MongoDB\BSON\toJSON;

class ControllerApp extends Controller
{
    //
    public function getAllPolitics(){
        $politics = Politics::all();
        return $politics->toJson();
    }

    public function getMascotaPolitic($id){
        //$mascota = Mascotes::find($id);
        $mascota = Mascotes::findOrFail($id);
        $politic = $mascota->politics()->first();

        //return $mascota->toJson();
        return "Mascota: " . $mascota->nom  . "<br/> Nom: " . $politic->nom . ", Cognoms: " . $politic->cognoms;
    }
    public function setCarrec($id,$carrec){
        $politic = Politics::findOrFail($id);
        $politic->carrec = $carrec;
        $politic->save();
        $politic = Politics::findOrFail($id);
        return $politic->toJson();
    }

    public function addEvent($nom){
        $event = new Esdeveniments();
        $event->nom=$nom;
        $event->save();
        $event2 = Esdeveniments::findOrFail($event->idEsdeveniment);
        return $event2->toJson();
    }

    public function getPoliticsCarrec($carrec){
        $politics = Politics::where('carrec',$carrec)->get();

        return $politics->toJson();
    }
    public function removeEvent($id){
        $return = Esdeveniments::destroy($id);
        return $return;
    }
    public function getPoliticsByNomCognoms($nom,$cognoms){
        //$politics = Politics::where('nom',$nom)->where('cognoms',$cognoms)->get();
        $politics = Politics::where('nom',$nom)->orWhere('cognoms',$cognoms)->get();
        return $politics->toJson();
    }
    public function getEventsPolitics($id)
    {
        $politic = Politics::find($id);
        $a = $politic->esdeveniments()->first();
        return $a->toJSON();
    }
    public function setEventPolitic($idPolitic,$idEvent){
        $politic = Politics::find($idPolitic);
        $event = Esdeveniments::find($idEvent);

        $politic->esdeveniments()->attach($event);


        $politic2 = Politics::find($politic->idPolitic);
        $events = $politic2->esdeveniments()->get();
        return $events->toJson();

    }
    public function getFormAllPolitics(){
        $tots=Politics::all();
        //return $tots->toJson();
        return view('getAllPolitics',compact('tots'));
    }
    public function getFormAllMascotes(){
        $tots=Mascotes::all();
        //return $tots->toJson();
        return view('getAllMascotes',compact('tots'));
    }

    public function dadesUsuari(){
        $bool = Auth::check();
        //devuelve el usuario registrado
        $userId = Auth::id();
        $user = Auth::user();
        //return $bool;
        //return $userId;

        return $user->toJson();

    }
    public function entregarInforme(){
        return view('entregarInforme');

    }
    ///viene de un post de un formulario con lo que tiene un request
    public function pujarInforme (Request $Request){
        //validacion, si no la cumple peta
        $Request->validate([
            //el campo file es obligatorio, debe ser extension pdf xlx o csv, tamaño maximo 2MB
            'file' => 'required|mimes:pdf,docx,odt|max:2048',
        ]);

        //nombre del fichero sacado a aprtir del time
        $fileName = time().'.'.$Request->file->extension();

        //mover el fichero a la capeta public/uploads con el nombre generado anteriorkmente
        $Request->file->move(public_path('uploads'), $fileName);

        $informe = new InformesPolitics();
        $informe->nom = $Request->nom;
        $informe->fitxer = $fileName;
        $informe->idPolitic = $Request->politic;
        $informe->userid = Auth::id();
        //la nueva variable path$fileName
        //$informe->ruta = public_path('uploads') . "\\" . $fileName ;
        $informe->save();

        //puedes añadirle un campo a un redirect o return
        //return $practica->toJson();
        return redirect('/meusInformes')->with('success','Informe entregat.','file',$fileName)
            ->with('file',$fileName);
    }

    public function meusInformes()
    {

        $user = Auth::user();

        $informes = InformesPolitics::Where('userid',Auth::id())->get();
        return view('meusInformes', compact('user'),compact('informes'));

    }
}
